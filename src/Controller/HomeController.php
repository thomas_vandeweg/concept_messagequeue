<?php

namespace App\Controller;

use App\Entity\Mail;
use App\Message\MailNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Messenger\MessageBusInterface;

class HomeController extends AbstractController
{
    public function index(Request $request, MessageBusInterface $bus): Response
    {
        $mail = new Mail();

        $form = $this->createFormBuilder($mail)
            ->add('emailAddress', TextType::class)
            ->add('firstName', TextType::class)
            ->add('lastName',TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Register'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // will cause the MailNotificationHandler to be called
            $bus->dispatch(new MailNotification($mail));

            return $this->redirectToRoute('success');
        }

        return $this->renderForm('home/index.html.twig', [
            'form' => $form,
        ]);
    }

    public function success(): Response
    {
        return $this->render('home/success.html.twig');
    }
}
