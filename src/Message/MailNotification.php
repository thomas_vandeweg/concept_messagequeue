<?php

namespace App\Message;

use App\Entity\Mail;

class MailNotification
{
    private $mail;

    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }
}
