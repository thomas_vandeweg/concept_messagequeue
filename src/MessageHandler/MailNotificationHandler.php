<?php

namespace App\MessageHandler;

use App\Message\MailNotification;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Email;

class MailNotificationHandler implements MessageHandlerInterface
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(MailNotification $message)
    {
        $mail = $message->getMail();

        sleep(10); // 10 seconds delay for demonstration that mail will be sent after registration

        $email = (new Email())
            ->from('e42a8e9829-0a901a@inbox.mailtrap.io')
            ->to($mail->getEmailAddress())
            ->subject('New registration!')
            ->html("New registration from: {$mail->getFirstName()} {$mail->getLastName()} {$mail->getEmailAddress()}");

        $this->mailer->send($email);
    }
}
